#include <stdlib.h>
#include <gtk/gtk.h>
#include <glib.h>

GtkWidget *status_bar;

static void push_item( GtkWidget *widget,
                       gpointer   data )
{
  static int count = 1;
  gchar *buff;

  buff = g_strdup_printf ("Item %d", count++);
  gtk_statusbar_push (GTK_STATUSBAR (status_bar), GPOINTER_TO_INT (data), buff);
  g_free (buff);
}

static void pop_item( GtkWidget *widget,
                      gpointer   data )
{
  gtk_statusbar_pop (GTK_STATUSBAR (status_bar), GPOINTER_TO_INT (data));
}

int main( int   argc,
          char *argv[] )
{
    GtkBuilder *builder;
    GtkWidget *window;
    GtkWidget *button;

    gint context_id;

    gtk_init (&argc, &argv);

    /* Construct a GtkBuilder instance and load our UI description */
    builder = gtk_builder_new ();
    gtk_builder_add_from_file (builder, "gui3.ui", NULL);

    /* Connect signal handlers to the constructed widgets. */
    window = gtk_builder_get_object (builder, "window");
    g_signal_connect (window, "destroy", G_CALLBACK (gtk_main_quit), NULL);


    /* create a new window */
    //window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    //gtk_widget_set_size_request (GTK_WIDGET (window), 200, 100);
    gtk_window_set_title (GTK_WINDOW (window), "GTK Statusbar Example");
    g_signal_connect (window, "delete-event",
                      G_CALLBACK (exit), NULL);
          
    status_bar = gtk_builder_get_object (builder, "statusbar1");     
    //gtk_box_pack_start (GTK_BOX (vbox), status_bar, TRUE, TRUE, 0);
    //gtk_widget_show (status_bar);

    context_id = gtk_statusbar_get_context_id(
                          GTK_STATUSBAR (status_bar), "Statusbar example");

    button = gtk_builder_get_object (builder, "settings");
    g_signal_connect (button, "clicked",
                      G_CALLBACK (push_item), GINT_TO_POINTER (context_id));
    //gtk_box_pack_start (GTK_BOX (vbox), button, TRUE, TRUE, 2);
    //gtk_widget_show (button);              





    /* always display the window as the last step so it all splashes on
     * the screen at once. */
    //gtk_widget_show (window);

    gtk_main ();

    return 0;
}
