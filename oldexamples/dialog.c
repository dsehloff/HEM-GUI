#include <gtk/gtk.h>

typedef struct _Data Data;
struct _Data
{
    GtkWidget *quit;
    GtkWidget *about;
};

G_MODULE_EXPORT gboolean
cb_delete_event( GtkWidget *window,
                 GdkEvent  *event,
                 Data      *data )
{
    gint response = 1;

    /* Run dialog */
    response = gtk_dialog_run( GTK_DIALOG( data->quit ) );
    gtk_widget_hide( data->quit );

    return( 1 != response );
}

G_MODULE_EXPORT void
cb_show_about( GtkButton *button,
               Data      *data )
{
    /* Run dialog */
    gtk_dialog_run( GTK_DIALOG( data->about ) );
    gtk_widget_hide( data->about );
}

int
main( int    argc,
      char **argv )
{
    GtkBuilder *builder;
    GtkWidget  *window;
    Data        data;

    gtk_init( &argc, &argv );

    /* Create builder and load interface */
    builder = gtk_builder_new();
    gtk_builder_add_from_file( builder, "dialogexample.builder", NULL );

    /* Obtain widgets that we need */
    window = GTK_WIDGET( gtk_builder_get_object( builder, "window1" ) );
    data.quit = GTK_WIDGET( gtk_builder_get_object( builder, "dialog1" ) );
    data.about = GTK_WIDGET( gtk_builder_get_object( builder, "aboutdialog1" ) );

    /* Connect callbacks */
    gtk_builder_connect_signals( builder, &data );

    /* Destroy builder */
    g_object_unref( G_OBJECT( builder ) );

    /* Show main window and start main loop */
    gtk_widget_show( window );
    gtk_main();

    return( 0 );
}
