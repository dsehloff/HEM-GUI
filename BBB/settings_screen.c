/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * settings.c
 * Copyright (C) 2017 David Sehloff <dsehloff@wisc.edu>
 * 
 * hem-gui is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * hem-gui is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <glib/gi18n.h>


#include "settings_screen.h"
#include "home_screen.h"
//#include "loads_screen.h"

GtkEntryBuffer *wifi_net_buffer;
GtkEntryBuffer *wifi_pass_buffer;
GtkEntryBuffer *phone_num_buffer;

/* --- specific get functions for current system values corresponding to each entry --- */
//TODO: Interface with HEMApp and connection manager to get the values of each
char * 
get_current_network(void)
{
	return "network";
}
char *
get_current_password(void)
{
	return "password";
}
char *
get_current_phone(void)
{
	return "5555555555";
}
/* ------------------------------------------------------------------------------------- */


/* -------------specific save functions for entry values--------------- */
//TODO: Interface with HEMApp and connection manager to save and set the values of each
int
save_network (const gchar *value)
{
	int success = 0;
		
	printf("Wi-Fi network: %s\n",value);
	success = 1;
	return success;

}

int
save_password (const gchar *value)
{
	int success = 0;

	printf("Wi-Fi password: %s\n",value);
	success = 1;
	return success;

}

int
save_phone (const gchar *value)
{
	int success = 0;

	printf("User Phone #: %s\n",value);
	success = 1;
	return success;

}

	
/* -------------------------------------------------------------------- */

/*
 * Function:  update_settings
 * --------------------
 * Updates the time in the settings screen
 */
int
update_settings(gpointer settings_priv)
{
  int success = 0;
  update_time(GTK_WIDGET(((SettingsPrivate*)settings_priv)->time_label));
  success = 1;
  return success;
}

/*
 * Function:  on_exit_button_clicked
 * --------------------
 * To be called when the exit button is clicked,
 * switches the screen to the home screen
 * by hiding the settings screen, freeing its variables, and presenting the home window
 */
void
on_exit_button_clicked (GtkWidget *widget, gpointer data)
{
	gtk_widget_hide(GTK_WIDGET(((SettingsPrivate*)data)->window));
    g_source_remove ((gint)((SettingsPrivate*)data)->timeout_tag);
	g_free ((SettingsPrivate*)data);	
	gtk_window_present((GtkWindow*)home_priv->window);
}

/*
 * Function:  on_save_and_exit_button_clicked
 * --------------------
 * To be called when the save and exit button is clicked,
 * calls the functions to save each of the values currently in the entry boxes,
 * then switches the screen to the home screen
 * by hiding the settings screen, freeing its variables, and presenting the home window
 */
int
on_save_and_exit_button_clicked (GtkWidget *widget, gpointer data)
{
	int success = 0;
	const gchar *entered_net_text = gtk_entry_get_text ((GtkEntry*)((SettingsPrivate*)data)->wifi_net_entry);
	int netstatus = save_network(entered_net_text);
	const gchar *entered_pass_text = gtk_entry_get_text ((GtkEntry*)((SettingsPrivate*)data)->wifi_pass_entry);
	int passstatus = save_password(entered_pass_text);
	const gchar *entered_num_text = gtk_entry_get_text ((GtkEntry*)((SettingsPrivate*)data)->phone_num_entry);
	int numstatus = save_phone(entered_num_text);
	if (netstatus && passstatus && numstatus)
	{
		success = 1;
	}
	gtk_widget_hide(GTK_WIDGET(((SettingsPrivate*)data)->window));
	g_source_remove ((gint)((SettingsPrivate*)data)->timeout_tag);
	g_free ((SettingsPrivate*)data);	
	gtk_window_present((GtkWindow*)home_priv->window);
	return success;
}

GtkWidget*
create_settings_window (void)
{
    /*---- CSS ------------------*/
    GtkCssProvider *provider;
    GdkDisplay *display;
    GdkScreen *screen;
    //GtkStyleContext *context;
    /*---------------------------*/

	char *init_network = get_current_network();
	char *init_password = get_current_password();
	char *init_phone = get_current_phone();

	wifi_net_buffer=gtk_entry_buffer_new(init_network,strlen(init_network));
	wifi_pass_buffer=gtk_entry_buffer_new(init_password,strlen(init_password));
	phone_num_buffer=gtk_entry_buffer_new(init_phone,strlen(init_phone));

	/* Get memory for structure of this window's widgets */
	settings_priv = g_malloc (sizeof (struct Settings_Private));

	GError* error = NULL;

	/* Load UI from file */
	settings_priv->builder = gtk_builder_new ();
	if (!gtk_builder_add_from_file (settings_priv->builder, UI_FILE_SETTINGS, &error))
	{
		g_critical ("Couldn't load builder file: %s", error->message);
		g_error_free (error);
	}

	/* Get the window object from the ui file */
	settings_priv->window = GTK_WIDGET (gtk_builder_get_object (settings_priv->builder, SETTINGS_WINDOW));
        if (!settings_priv->window)
        {
                g_critical ("Widget \"%s\" is missing in file %s.",
				SETTINGS_WINDOW,
				UI_FILE_SETTINGS);
        }
	settings_priv->scrollwindow = (GtkScrolledWindow*)(gtk_builder_get_object(settings_priv->builder, "scrolledwindow"));
	gtk_widget_set_vexpand((GtkWidget *)settings_priv->scrollwindow,TRUE);
	settings_priv->exitbutton = GTK_WIDGET (gtk_builder_get_object (settings_priv->builder, "exit_button"));
	settings_priv->save_and_exitbutton = GTK_WIDGET (gtk_builder_get_object (settings_priv->builder, "save_exit_button"));

	settings_priv->time_label = GTK_WIDGET(gtk_builder_get_object(settings_priv->builder, "time_label"));


    /* Add a grid to the scrolled window */
    settings_priv->grid=gtk_grid_new();
	gtk_grid_set_row_spacing ((GtkGrid*)(settings_priv->grid),7);
	gtk_grid_set_column_spacing ((GtkGrid*)(settings_priv->grid),5);
    gtk_container_add (GTK_CONTAINER(settings_priv->scrollwindow), settings_priv->grid);

    settings_priv->wifi_net_label=gtk_label_new("Wi-Fi\nNetwork");
	settings_priv->wifi_net_entry=gtk_entry_new_with_buffer (wifi_net_buffer);

	/* attach the label and entry box to the corresponding row in the grid */
    gtk_grid_attach ((GtkGrid*)settings_priv->grid, settings_priv->wifi_net_label,0,1,1,1);
    gtk_grid_attach ((GtkGrid*)settings_priv->grid, settings_priv->wifi_net_entry,1,1,1,1);

	settings_priv->wifi_pass_label=gtk_label_new("Wi-Fi\nPassword");
	settings_priv->wifi_pass_entry=gtk_entry_new_with_buffer (wifi_pass_buffer);

	/* attach the label and entry box to the corresponding row in the grid */
    gtk_grid_attach ((GtkGrid*)settings_priv->grid, settings_priv->wifi_pass_label,0,2,1,1);
    gtk_grid_attach ((GtkGrid*)settings_priv->grid, settings_priv->wifi_pass_entry,1,2,1,1);

	settings_priv->phone_num_label=gtk_label_new("User\nPhone #");
	settings_priv->phone_num_entry=gtk_entry_new_with_buffer (phone_num_buffer);

	/* attach the label and entry box to the corresponding row in the grid */
    gtk_grid_attach ((GtkGrid*)settings_priv->grid, settings_priv->phone_num_label,0,3,1,1);
    gtk_grid_attach ((GtkGrid*)settings_priv->grid, settings_priv->phone_num_entry,1,3,1,1);

	
    /* Connect callbacks */
    //g_signal_connect (loads_priv->updatebutton, "clicked",G_CALLBACK (on_loadupdate_button_clicked),(gpointer)loads_priv);
	g_signal_connect (settings_priv->exitbutton, "clicked",G_CALLBACK (on_exit_button_clicked),(gpointer)settings_priv);
	g_signal_connect (settings_priv->save_and_exitbutton, "clicked",G_CALLBACK (on_save_and_exit_button_clicked),(gpointer)settings_priv);
    g_signal_connect (settings_priv->window, "destroy", G_CALLBACK (gtk_main_quit), NULL);


	/* Initialize value of time */
	update_settings((gpointer)settings_priv);


	/* Start Periodic Updates */
	settings_priv->timeout_tag=g_timeout_add(UPDATE_INTERVAL_MS, (GSourceFunc)update_settings, (gpointer)settings_priv);


    /* -----------------CSS----------------------------------------------------------------------------------*/
    provider = gtk_css_provider_new ();
    display = gdk_display_get_default ();
    screen = gdk_display_get_default_screen (display);
    gtk_style_context_add_provider_for_screen (screen, GTK_STYLE_PROVIDER (provider), GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
 
    const gchar* home = "style.css";
 
    
    gtk_css_provider_load_from_file(provider, g_file_new_for_path(home), &error);
    g_object_unref (provider);
    /*--------------------------------------------------------------------------------------------------------*/
    
    /* For touchscreen, set fullscreen */
    gtk_window_fullscreen ((GtkWindow*)settings_priv->window);


    /* Destroy builder */
    g_object_unref( G_OBJECT( settings_priv->builder ) );

	return settings_priv->window;
}

