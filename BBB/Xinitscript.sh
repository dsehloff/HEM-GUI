#!/usr/bin/env bash
procx=$(ps -e | grep X)
counter=0
while [ ! -n "$procx" ]; do
    #echo Trying Initialization    
    FRAMEBUFFER=/dev/fb1 xinit &
    sleep 1
    #echo First check
    i=0
    procx=$(ps -e | grep X)
    #ps -e | grep X
    while [[ $procx == *"?"* ]]; do
        sleep 0.5
        let i=i+1
        if [ "$i" -gt "240" ]; then
            break
        fi
        procx=$(ps -e | grep X)
        #echo Defunct loop check
        #ps -e | grep X
    done
    #sleep 5
    procx=$(ps -e | grep X)
    #echo Final check
    #ps -e | grep X
    let counter=counter+1
    if [ "$counter" -gt "10" ]; then
        break
    fi
done
echo Initialized X server
