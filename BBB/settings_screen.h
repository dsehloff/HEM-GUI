/* struct to store all the Gtk elements specific to the settings screen */

#ifndef _SETTINGSH_
#define _SETTINGSH_

#define UI_FILE_SETTINGS "settings.ui"
#define SETTINGS_WINDOW "settings_window"

typedef struct Settings_Private SettingsPrivate;
struct Settings_Private
{
	GtkWidget *window;
	GtkBuilder *builder;
	GtkScrolledWindow *scrollwindow;
	GtkWidget *exitbutton;
	GtkWidget *save_and_exitbutton;
	GtkWidget *grid;
	GtkWidget *time_label;
	GtkWidget *wifi_net_label;
	GtkWidget *wifi_net_entry;
	GtkWidget *wifi_pass_label;
	GtkWidget *wifi_pass_entry;
	GtkWidget *phone_num_label;
	GtkWidget *phone_num_entry;
    gint timeout_tag;
	
};


SettingsPrivate* settings_priv;

int
update_settings(gpointer loads_priv);

void
on_exit_button_clicked (GtkWidget *widget, gpointer data);

char * 
get_current_network(void);

char *
get_current_password(void);

char *
get_current_phone(void);

int
save_network (const gchar *value);

int
save_password (const gchar *value);

int
save_phone (const gchar *value);

GtkWidget* create_settings_window (void);

#endif
