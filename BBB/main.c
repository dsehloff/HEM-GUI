#include "main.h"

int
main (int argc, char *argv[])
{
 	GtkWidget *window;



#ifdef ENABLE_NLS

	bindtextdomain (GETTEXT_PACKAGE, PACKAGE_LOCALE_DIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);
#endif
	
    //Initialize the Adafruit 2.8 TFT display
//    FILE *fp;
//    int status;
//    char path[PATH_MAX];
    char cwd[1000];
    char lcd[1024];
    char xserver[1024];
    if (getcwd(cwd, sizeof(cwd)) != NULL){
       fprintf(stdout, "Current working dir: %s\n", cwd);
    }
    else{
       perror("getcwd() error");
    }
    strcpy(lcd,cwd);
    strcat(lcd,"/lcdinit.sh");

    printf("Initializing LCD\n");
    system(lcd);

//    fp = popen(lcd, "r");
//    if (fp == NULL){
    /* Handle error */
//    }
//    while (fgets(path, PATH_MAX, fp) != NULL){
//        printf("%s", path);
//    }
//    status = pclose(fp);
//    if (status == -1) {
//        printf("Error reported in lcd initialization\n");
        /* Error reported by pclose() */
//    } else {
//        printf("Status: %d\n",status);
    /* Use macros described under wait() to inspect `status' in order
       to determine success/failure of command executed by popen() */
//    }
    strcpy(xserver,cwd);
    strcat(xserver,"/Xinitscript.sh");


//    time_t start, end;
//    double elapsed;  // seconds
//    start = time(NULL);
//    int terminate = 1;
//    while (terminate) {
//     end = time(NULL);
//     elapsed = difftime(end, start);
     
//     fp = popen("lsmod | grep ft", "r");
//    if (fp == NULL){
    /* Handle error */
//    }
//    while (fgets(path, PATH_MAX, fp) != NULL){
//        printf("%s", path);
//        strcpy(pathval,path);
//    }
    //whether pathval is empty will show if x is running
//    printf("%s", pathval);
    
//    status = pclose(fp);
//    if (status == -1) {
//        printf("Error reported in X initialization\n");
//        /* Error reported by pclose() */
//    } else {
//        printf("Status: %d\n",status);
    /* Use macros described under wait() to inspect `status' in order
       to determine success/failure of command executed by popen() */
//    }

//     if (elapsed >= 0.1 /* seconds */)
//       terminate = 0;
//     else  // No need to sleep when 90.0 seconds elapsed.
//       usleep(10);
//    }

    sleep(5);
    printf("Initializing XServer\n");
    //initialize x server
    system(xserver);

    putenv("DISPLAY=:0.0");
    putenv("NO_AT_BRIDGE=1");


	gtk_init (&argc, &argv);
    printf("GTK Initialized\n");
	window = create_home_window ();
	gtk_widget_show_all (window);
    
    GdkDisplay * display = gdk_display_get_default ();
    GdkScreen *screen = gdk_display_get_default_screen (display);
	GdkWindow * win = gdk_screen_get_root_window(screen);
	
    gdk_window_set_cursor(win,gdk_cursor_new_for_display(gdk_display_get_default(),GDK_BLANK_CURSOR));


	gtk_main ();

	g_free (home_priv);


	return 0;
}

