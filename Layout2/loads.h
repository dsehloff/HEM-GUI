#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define NUM_LOADS 8
#define LOAD_CHARS 10
#define PWR_CHARS 8

void get_meter_data(void); //update status variables
static void print_data(void); //print status to labels on screen
void relay_update(void); //update status of relay toggles on screen
void update_data(void); //call all update functions
void relay_toggled(GtkWidget *toggle, gpointer data); //function for changing relay status when a relay button is toggled
int load_window(GtkWidget *widget); //window for loads
