#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <stdbool.h>
#include "loads.h"

//global Gtk Variables for each widget
GtkWidget *status_hemapp;
GtkWidget *status_wifi;
GtkLevelBar *status_wifi_signal;
GtkWidget *status_gsm;
GtkWidget *status_mobiledata;
GtkWidget *status_i2c;
GtkWidget *status_uart;
GtkWidget *status_memcloud;
GtkWidget *status_load1;
GtkWidget *status_load2;
GtkWidget *status_load3;
GtkWidget *status_load4;
GtkWidget *time_label;
guint context_id[11];
//global struct for all the HEM status variables
struct status_data{
  bool hemapp_running;
  char *wifi_network;
  int wifi_signal;
  bool gsm_connected;
  bool mobiledata_connected;
  char *i2c_enabled;
  bool uart_enabled;
  bool memcloud_connected;
  int hour;
  int minute;
  
};
//globally define a specific instance
struct status_data data;

/*
 * Function:  get_status_data 
 * --------------------
 * updates the struct data with the current status of each component 
 * to display on the status screen
 */
void get_status_data()
{
  data.hemapp_running=TRUE;
  data.wifi_network="eduroam";
  data.wifi_signal=64;
  data.gsm_connected=TRUE;
  data.mobiledata_connected=TRUE;
  data.i2c_enabled="I2C1,I2C2";
  data.uart_enabled=TRUE;
  data.memcloud_connected=TRUE;
  data.hour=7;
  data.minute=0;
}

/*
 * Function:  push_status
 * --------------------
 * gets the latest status values and prints meaningful verbal representations to the statusbars
 * static because of updating count, which is used only for testing
 */
static void push_status()
{
  get_status_data();
  gchar *buff;
  gchar *hourstr;
  gchar *minutestr;
  static int count = 0;
  
  data.minute= (++count) % 60;

  if (data.hemapp_running) {
    buff = g_strdup_printf ("running");
    gtk_statusbar_push (GTK_STATUSBAR (status_hemapp), context_id[0], buff);
    g_free (buff);
  }
  else {
    buff = g_strdup_printf ("not running");
    gtk_statusbar_push (GTK_STATUSBAR (status_hemapp), context_id[0], buff);
    g_free (buff);
  }
  buff = g_strdup_printf ("%s",data.wifi_network);
  gtk_statusbar_push (GTK_STATUSBAR (status_wifi), context_id[1], buff);
  g_free (buff);
  gtk_level_bar_set_value (status_wifi_signal,data.wifi_signal);
  if (data.gsm_connected) {
    buff = g_strdup_printf ("connected");
    gtk_statusbar_push (GTK_STATUSBAR (status_gsm), context_id[3], buff);
    g_free (buff);
  }
  else {
    buff = g_strdup_printf ("not connected");
    gtk_statusbar_push (GTK_STATUSBAR (status_gsm), context_id[3], buff);
    g_free (buff);
  }
  if (data.mobiledata_connected) {
    buff = g_strdup_printf ("connected");
    gtk_statusbar_push (GTK_STATUSBAR (status_mobiledata), context_id[4], buff);
    g_free (buff);
  }
  else {
    buff = g_strdup_printf ("not connected");
    gtk_statusbar_push (GTK_STATUSBAR (status_mobiledata), context_id[4], buff);
    g_free (buff);
  }
  buff = g_strdup_printf ("%s",data.i2c_enabled);
  gtk_statusbar_push (GTK_STATUSBAR (status_i2c), context_id[5], buff);
  g_free (buff);
  if (data.uart_enabled) {
    buff = g_strdup_printf ("enabled");
    gtk_statusbar_push (GTK_STATUSBAR (status_uart), context_id[6], buff);
    g_free (buff);
  }
  else {
    buff = g_strdup_printf ("not connected");
    gtk_statusbar_push (GTK_STATUSBAR (status_uart), context_id[6], buff);
    g_free (buff);
  }
  if (data.memcloud_connected) {
    buff = g_strdup_printf ("connected");
    gtk_statusbar_push (GTK_STATUSBAR (status_memcloud), context_id[7], buff);
    g_free (buff);
  }
  else {
    buff = g_strdup_printf ("not connected");
    gtk_statusbar_push (GTK_STATUSBAR (status_memcloud), context_id[7], buff);
    g_free (buff);
  }
  if (data.hour<10){
    hourstr = g_strdup_printf ("0%d",data.hour);
  }
  else {
    hourstr = g_strdup_printf ("%d",data.hour);
  }
  if (data.minute<10){
    minutestr = g_strdup_printf ("0%d",data.minute);
  }
  else {
    minutestr = g_strdup_printf ("%d",data.minute);
  }
  buff = g_strdup_printf ("%s:%s",hourstr,minutestr);
  g_free (hourstr);
  g_free (minutestr);
  gtk_label_set_text (GTK_LABEL (time_label),buff);
  g_free (buff);

}

/*
 * Function:  pop_status
 * --------------------
 * remove the current status value from each statusbar
 */
void pop_status()
{
  gtk_statusbar_pop (GTK_STATUSBAR (status_hemapp), context_id[0]);
  gtk_statusbar_pop (GTK_STATUSBAR (status_wifi), context_id[1]);
  gtk_statusbar_pop (GTK_STATUSBAR (status_gsm), context_id[2]);
  gtk_statusbar_pop (GTK_STATUSBAR (status_mobiledata), context_id[3]);
  gtk_statusbar_pop (GTK_STATUSBAR (status_i2c), context_id[4]);
  gtk_statusbar_pop (GTK_STATUSBAR (status_uart), context_id[5]);
  gtk_statusbar_pop (GTK_STATUSBAR (status_memcloud), context_id[6]);
}

/*
 * Function:  update_status
 * --------------------
 * pop and push to remove the previous status value from each statusbar
 * and replace it with a new one
 */
void update_status()
{
  pop_status();
  push_status();
}

int
main( int    argc,
      char **argv )
{
    GtkBuilder *builder;
    GtkWidget  *window;
    GtkWidget *update_button;
    GtkWidget *loads_button;

    /*---- CSS ------------------*/
    GtkCssProvider *provider;
    GdkDisplay *display;
    GdkScreen *screen;
    /*---------------------------*/

    gtk_init( &argc, &argv );  

    /* Create builder and load interface */
    builder = gtk_builder_new();
    gtk_builder_add_from_file( builder, "layout2.glade", NULL );

    /* Obtain widgets that we need */
    window = GTK_WIDGET(gtk_builder_get_object(builder, "window1"));
    update_button = GTK_WIDGET(gtk_builder_get_object(builder, "update_button"));
    loads_button = GTK_WIDGET(gtk_builder_get_object(builder, "load_button"));
    status_hemapp = GTK_WIDGET(gtk_builder_get_object( builder, "hemapp"));
    status_wifi = GTK_WIDGET(gtk_builder_get_object( builder, "wifi_network"));
    status_wifi_signal = (GtkLevelBar*)(gtk_builder_get_object( builder, "wifi_signal"));
    status_gsm = GTK_WIDGET(gtk_builder_get_object( builder, "gsm"));
    status_mobiledata = GTK_WIDGET(gtk_builder_get_object( builder, "mobiledata"));
    status_i2c = GTK_WIDGET(gtk_builder_get_object( builder, "i2c"));
    status_uart = GTK_WIDGET(gtk_builder_get_object( builder, "uart"));
    status_memcloud = GTK_WIDGET(gtk_builder_get_object( builder, "memcloud"));
    time_label = GTK_WIDGET(gtk_builder_get_object( builder, "label8"));
    /*
    status_load1 = GTK_WIDGET(gtk_builder_get_object( builder, "load1"));\
    status_load2 = GTK_WIDGET(gtk_builder_get_object( builder, "load2"));
    status_load3 = GTK_WIDGET(gtk_builder_get_object( builder, "load3"));
    status_load4 = GTK_WIDGET(gtk_builder_get_object( builder, "load4"));    
    */

    /* Get Context ID for Status Bar */
    context_id[0] = gtk_statusbar_get_context_id(GTK_STATUSBAR (status_hemapp), "HEMApp Status");
    context_id[1] = gtk_statusbar_get_context_id(GTK_STATUSBAR (status_wifi), "Wi-Fi Status");
    context_id[2] = gtk_statusbar_get_context_id(GTK_STATUSBAR (status_gsm), "GSM Status");
    context_id[3] = gtk_statusbar_get_context_id(GTK_STATUSBAR (status_mobiledata), "Mobile Data Status");
    context_id[4] = gtk_statusbar_get_context_id(GTK_STATUSBAR (status_i2c), "I2C Status");
    context_id[5] = gtk_statusbar_get_context_id(GTK_STATUSBAR (status_uart), "UART Status");
    context_id[6] = gtk_statusbar_get_context_id(GTK_STATUSBAR (status_memcloud), "MEMCloud Status");


/*
    context_id[7] = gtk_statusbar_get_context_id(GTK_STATUSBAR (status_load1), "Load 1 Status");
    context_id[8] = gtk_statusbar_get_context_id(GTK_STATUSBAR (status_load2), "Load 2 Status");
    context_id[9] = gtk_statusbar_get_context_id(GTK_STATUSBAR (status_load3), "Load 3 Status");
    context_id[10] = gtk_statusbar_get_context_id(GTK_STATUSBAR (status_load4), "Load 4 Status");
*/

   
    /* Connect callbacks */
    g_signal_connect (update_button, "clicked",G_CALLBACK (update_status), NULL);
    g_signal_connect (window, "destroy", G_CALLBACK (gtk_main_quit), NULL);

    g_signal_connect (loads_button, "clicked", G_CALLBACK (load_window), (gpointer) window);





    /* ----------------- CSS ----------------------------------------------------------------------------------------------*/
    provider = gtk_css_provider_new ();
    display = gdk_display_get_default ();
    screen = gdk_display_get_default_screen (display);
    gtk_style_context_add_provider_for_screen (screen, GTK_STYLE_PROVIDER (provider), GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
 
    const gchar* home = "style.css";
 
    GError *error = 0;
 
    gtk_css_provider_load_from_file(provider, g_file_new_for_path(home), &error);
    g_object_unref (provider);
    /* --------------------------------------------------------------------------------------------------------------------*/
    
    
    /* Destroy builder */
    g_object_unref( G_OBJECT( builder ) );

    /* Show main window and start main loop */
    gtk_widget_show( window );

    /* For touchscreen only */
    //gtk_window_fullscreen (window);
    //gdk_window_set_cursor(gtk_widget_get_window(GTK_WIDGET(gtk_window)),gdk_cursor_new(GDK_BLANK_CURSOR));
    gtk_main();

    return( 0 );
}
